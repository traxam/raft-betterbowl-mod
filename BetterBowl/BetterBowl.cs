﻿using HarmonyLib;
using UnityEngine;
using System.Reflection;

namespace BetterBowl
{
    public class BetterBowlMod : Mod
    {
        private static BetterBowlMod _instance;

        /// <summary>
        /// The harmony identifier.
        /// </summary>
        private const string HarmonyId = "am.trax.betterbowl";

        /// <summary>
        /// The PlayerPrefs key for the 'current uses' variable.
        /// </summary>
        private const string PpCurrentUses = "TR.BETTERBOWL.CURRENTUSES";

        /// <summary>
        /// The PlayerPrefs key for the 'goal uses' variable.
        /// </summary>
        private const string PpGoalUses = "TR.BETTERBOWL.GOALUSES";

        /// <summary>
        /// The PlayerPrefs key for the 'min uses' variable.
        /// </summary>
        private const string PpMinUses = "TR.BETTERBOWL.MINUSES";

        /// <summary>
        /// The PlayerPrefs key for the 'max uses' variable.
        /// </summary>
        private const string PpMaxUses = "TR.BETTERBOWL.MAXUSES";

        private Harmony _harmony;
        private int _minUses;
        private int _maxUses;
        private int _currentUses;
        private int _goalUses;

        #region Startup and shutdown

        public void Start()
        {
            _instance = this;
            ComponentManager<BetterBowlMod>.Value = this;

            _harmony = new Harmony(HarmonyId);
            _harmony.PatchAll(Assembly.GetExecutingAssembly());

            LoadState();

            LogInfo("BetterBowl has been loaded!");
        }

        public void OnModUnload()
        {
            _harmony.UnpatchAll(HarmonyId);

            ComponentManager<BetterBowlMod>.Value = null;

            LogInfo("BetterBowl has been unloaded!");
        }

        #endregion

        #region Bowl return

        /// <summary>
        /// Method that should be called when an item is consumed.
        /// </summary>
        /// <param name="item">the item that was consumed</param>
        public void OnConsumeItem(Item_Base item)
        {
            if (IsBowlItem(item))
            {
                ReturnBowl();
            }
        }

        /// <summary>
        /// Returns a bowl to the local player, if it does not break.
        /// </summary>
        private void ReturnBowl()
        {
            _currentUses++;
            if (_currentUses >= _goalUses)
            {
                ResetCurrentUses();
            }
            else
            {
                RAPI.GetLocalPlayer().Inventory.AddItem(ItemManager.GetItemByName("Claybowl_Empty").UniqueName, 1);
            }

            SaveState();
        }

        /// <summary>
        /// Resets the current uses and generates a new goalUses value, based on
        /// minUses and maxUses. Does not save the state.
        /// </summary>
        private void ResetCurrentUses()
        {
            _currentUses = 0;
            _goalUses = Random.Range(_minUses, _maxUses);
        }

        /// <summary>
        /// Checks whether an item contains a bowl.
        /// </summary>
        /// <param name="item">the item to check</param>
        /// <returns>true if the given item contains a bowl and false otherwise</returns>
        private static bool IsBowlItem(Item_Base item)
        {
            return item.name.Contains("Claybowl") || item.name.Contains("ClayPlate");
        }

        #endregion

        #region Commands

        /// <summary>
        /// Parses and handles the 'breakInt' command.
        /// </summary>
        [ConsoleCommand("breakInt", "Sets the interval in which the bowl breaks.")]
        // ReSharper disable once UnusedMember.Global
        public static void HandleBreakIntCommand(string[] arguments)
        {
            if (arguments.Length == 0)
            {
                LogInfo("The current break interval is <color=green>" + _instance._minUses + " - " +         // ReSharper disable once UnusedMember.Local
                        _instance._maxUses + "</color>.");
                LogFollowUp(
                    "Type <color=green>breakInt <minUses> <maxUses></color> to change the break interval, i.e. <color=green>breakInt 5 10</color> (3 is the default min-uses value and 5 is the default max-uses value).");
            }
            else if (arguments.Length != 2)
            {
                LogError("Invalid syntax!");
                LogFollowUp(
                    "Type <color=green>breakInt <minUses> <maxUses></color> to change the break interval, i.e. <color=green>breakInt 5 10</color> (3 is the default min-uses value and 5 is the default max-uses value).");
            }
            else
            {
                // parse int values
                if (!int.TryParse(arguments[0], out var newMinUses))
                {
                    LogError("<color=green>" + arguments[0] + "</color> is not a valid integer value.");
                    return;
                }

                if (!int.TryParse(arguments[1], out var newMaxUses))
                {
                    LogError("<color=green>" + arguments[1] + "</color> is not a valid integer value.");
                    return;
                }

                // check range of new values
                if (newMinUses < 0)
                {
                    LogError("<color=green><minUses></color> must not be a negative value.");
                    return;
                }
                else if (newMaxUses < 0)
                {
                    LogError("<color=green><maxUses></color> must not be a negative value.");
                    return;
                }
                else if (newMaxUses < newMinUses)
                {
                    LogError("<color=green><maxUses></color> can not be less than <color=green><minUses></color>.");
                    return;
                }

                // apply new values
                _instance._minUses = newMinUses;
                _instance._maxUses = newMaxUses;
                _instance.ResetCurrentUses();
                _instance.SaveState();
                LogInfo("The break interval was set to <color=green>" + _instance._minUses + " - " + _instance._maxUses + "</color>.");
            }
        }

        #endregion

        #region Save and Load

        /// <summary>
        /// Loads the state (bowl usage and settings data) from PlayerPrefs.
        /// </summary>
        private void LoadState()
        {
            _minUses = PlayerPrefs.GetInt(PpMinUses, 3);
            _maxUses = PlayerPrefs.GetInt(PpMaxUses, 5);
            _currentUses = PlayerPrefs.GetInt(PpCurrentUses, 0);
            _goalUses = PlayerPrefs.GetInt(PpGoalUses, 4);
            LogInfo("The current break interval is <color=green>" + _minUses + " - " + _maxUses + "</color>.");
        }

        /// <summary>
        /// Saves the state (bowl usage and settings data) to PlayerPrefs.
        /// </summary>
        private void SaveState()
        {
            PlayerPrefs.SetInt(PpMinUses, this._minUses);
            PlayerPrefs.SetInt(PpMaxUses, this._maxUses);
            PlayerPrefs.SetInt(PpCurrentUses, this._currentUses);
            PlayerPrefs.SetInt(PpGoalUses, this._goalUses);
            PlayerPrefs.Save();
        }

        #endregion

        #region Logging

        /// <summary>
        /// Logs an informative message to the console.
        /// </summary>
        private static void LogInfo(string message)
        {
            Debug.Log("<color=#3498db>[info]</color>\t<b>Better Bowl mod:</b> " + message);
        }

        /// <summary>
        /// Logs an error message to the console.
        /// </summary>
        private static void LogError(string message)
        {
            Debug.LogError("<color=#e74c3c>[error]</color>\t<b>Better Bowl mod:</b> " + message);
        }

        /// <summary>
        /// Logs an follow-up message (without prefix) to the console.
        /// </summary>
        private static void LogFollowUp(string message)
        {
            Debug.Log("\t" + message);
        }

        #endregion
    }

    #region Patches

    /// <summary>
    /// This patch hooks into the item consumption event to trigger the bowl return.
    /// </summary>
    [HarmonyPatch(typeof(PlayerStats))]
    [HarmonyPatch("Consume")]
    // ReSharper disable once UnusedType.Global
    internal class PatchPlayerStatsConsume
    {
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        private static void Postfix(PlayerStats __instance, Item_Base edibleItem)
        {
            if (__instance.GetComponent(typeof(Network_Player)) == RAPI.GetLocalPlayer())
            {
                ComponentManager<BetterBowlMod>.Value.OnConsumeItem(edibleItem);
            }
        }
    }

    #endregion
}